import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.math.BigInteger;

public class Layouts extends LinearProgramming{
    private Stage window;
    public Layouts(Stage window){
        this.window = window;
    }

    public void loadScreen1(){
        window.setTitle("Skaitiniai ir optimizavimo metodai");
        VBox layout = new VBox(10);
        Label label = new Label("Pasirinkite funkciją");
        label.setPadding(new Insets(0, 0, 30, 0));
        Button btnDaugyba = new Button("Sudauginti dvi matricas A*B");
        btnDaugyba.setMinWidth(250);
        Button btnAtvMatrica = new Button("Surasti atvirkštinę matricą A^(-1)");
        btnAtvMatrica.setMinWidth(250);
        Button btnDeterminant = new Button("Apskaičiuoti matricos determinantą");
        btnDeterminant.setMinWidth(250);
        Button btnFunkc1 = new Button("Apskaičiuoti: X = A^(-1) * B");
        Button btnFunkc2 = new Button("Apskaičiuoti: X = B * A^(-1)");
        Button btnFunkc3 = new Button("Apskaičiuoti: X = A^(-1) * C * B^(-1)");
        btnFunkc1.setMinWidth(250);
        btnFunkc2.setMinWidth(250);
        btnFunkc3.setMinWidth(250);

        btnDaugyba.setOnAction(e -> {

            //Scene scena = daugybosRezScene(3, 4, 3, 4);
            Scene scena = daugybosDimScene();
            window.setScene(scena);
            window.setTitle("Skaitiniai ir optimizavimo metodai - daugyba");
        });
        btnFunkc3.setOnAction(e -> {
            Scene scena = x3DimsScene();
            window.setScene(scena);
            window.setTitle("Skaitiniai ir optimizavimo metodai - Funkcija3.  X = A^(-1) * C * B^(-1)");
        });

        btnFunkc2.setOnAction(e -> {
            Scene scena = x2DimsScene();
            window.setScene(scena);
            window.setTitle("Skaitiniai ir optimizavimo metodai - Funkcija2.  X = B * A^(-1)");
        });

        btnFunkc1.setOnAction(e -> {
            Scene scena = x1DimsScene();
            window.setScene(scena);
            window.setTitle("Skaitiniai ir optimizavimo metodai - Funkcija1.  X = A^(-1) * B");
        });

        btnDeterminant.setOnAction(e -> {
            Scene scena = determinantDimScene();
            window.setScene(scena);
            window.setTitle("Skaitiniai ir optimizavimo metodai - Determinantas");
        });

        btnAtvMatrica.setOnAction(e -> {
            Scene scena = inverseDimScene();
            window.setScene(scena);
            window.setTitle("Skaitiniai ir optimizavimo metodai - Atvirkštinė Matrica");
        });

        layout.getChildren().addAll(label, btnDaugyba, btnAtvMatrica, btnDeterminant, btnFunkc1, btnFunkc2, btnFunkc3);
        layout.setAlignment(Pos.TOP_CENTER);
        layout.setPadding(new Insets(20, 0, 0, 0));

        Scene scene = new Scene(layout);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");

        //window.resizableProperty().setValue(Boolean.FALSE);
        window.setScene(scene);
        window.show();
    }

    public Scene inverseRezScene(int Am, int An){
        TextField[][] tf1 = new TextField[Am][An];
        TextField[][] tfRez = new TextField[Am][An];
        GridPane mat1 = createTable(Am, An, tf1);
        GridPane rez = createTable(Am, An, true, tfRez);

        HBox hBox1 = createTable("Matrica A", Am, An, mat1);

        HBox finalHBox = new HBox();
        finalHBox.getChildren().addAll(hBox1);
        finalHBox.setAlignment(Pos.CENTER);

        Button btnSkaiciuoti = createButton("Skaičiuoti");
        HBox bottom = createBottom(btnSkaiciuoti, rez);
        bottom.setAlignment(Pos.CENTER);
        VBox vBoxBottom = new VBox();
        vBoxBottom.setAlignment(Pos.CENTER);
        vBoxBottom.setSpacing(40);

        Button btnAtgal = createButton("<<Atgal");
        VBox v = new VBox(btnAtgal);
        vBoxBottom.getChildren().addAll(finalHBox, bottom, v);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBoxBottom);
        scrollPane.setStyle("-fx-background-color:transparent;");

        HBox vb = new HBox(scrollPane);
        vb.setAlignment(Pos.CENTER);

        vb.setPadding(new Insets(40, 0, 0, 0));
        BorderPane layout = createBorderPane("Įveskite matricų A ir B duomenis į atitinkamus laukus", vb);
        layout.setAlignment(scrollPane, Pos.CENTER);

        btnSkaiciuoti.setOnAction(e -> {
            BigRational[][] matA = getElementsOfGridPane(tf1, Am, An);
            if(matA == null){
                new AlertBox("Neteisingai įvesti Matricos duomenys.");
            } else {
                BigRational[][] result = super.inverse2(matA);
                printResultToGridPane(result, tfRez);
            }
        });

        btnAtgal.setOnAction(e -> {
            loadScreen1();
        });

        Scene scene = new Scene(layout);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene inverseDimScene(){
        ComboBox<Integer> comboBoxMatAm = createComboBox();
        ComboBox<Integer> comboBoxMatAn = createComboBox();

        comboBoxMatAm.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatAm.getValue());
        });
        comboBoxMatAn.setOnAction(e -> {
            comboBoxMatAm.setValue(comboBoxMatAn.getValue());
        });
        comboBoxMatAn.setDisable(true);


        BorderPane border1 = createMatricosDimBorderPane("Matrica A", comboBoxMatAm, comboBoxMatAn, 5);

        Button btnAtgal = createButton("<Atgal");
        Button btnPirmyn = createButton("Pirmyn");

        VBox v = new VBox();
        HBox centerHBox = new HBox();
        centerHBox.setSpacing(30);
        centerHBox.setAlignment(Pos.TOP_CENTER);
        centerHBox.getChildren().addAll(border1);

        VBox vBox = new VBox(btnAtgal);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(70,0,0,0));
        v.setSpacing(30);
        v.getChildren().addAll(centerHBox, btnPirmyn, vBox);
        v.setAlignment(Pos.TOP_CENTER);
        v.setPadding(new Insets(70, 0, 0, 0));


        btnAtgal.setOnAction(e -> {
            loadScreen1();
        });
        btnPirmyn.setOnAction(e -> {
            int value = comboBoxMatAm.getValue();
            window.setScene(inverseRezScene(value, value));
        });


        HBox vb = new HBox(v);
        vb.setAlignment(Pos.CENTER);
        BorderPane layout = createBorderPane("Pasirinkite matricos A eilučių kiekį (m) bei stulpelių kiekį (n)", vb);
        Scene scene = new Scene(layout);

        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene determinantRezScene(int Am, int An){
        TextField[][] tf1 = new TextField[Am][An];
        TextField[][] tfRez = new TextField[1][1];
        GridPane mat1 = createTable(Am, An, tf1);
        GridPane rez = createTable(1, 1, true, tfRez);

        HBox hBox1 = createTable("Matrica A", Am, An, mat1);

        HBox finalHBox = new HBox();
        finalHBox.getChildren().addAll(hBox1);
        finalHBox.setAlignment(Pos.CENTER);

        Button btnSkaiciuoti = createButton("Skaičiuoti");
        HBox bottom = createBottom(btnSkaiciuoti, rez);
        bottom.setAlignment(Pos.CENTER);
        VBox vBoxBottom = new VBox();
        vBoxBottom.setAlignment(Pos.CENTER);
        vBoxBottom.setSpacing(40);

        Button btnAtgal = createButton("<<Atgal");
        VBox v = new VBox(btnAtgal);
        vBoxBottom.getChildren().addAll(finalHBox, bottom, v);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBoxBottom);
        scrollPane.setStyle("-fx-background-color:transparent;");

        HBox vb = new HBox(scrollPane);
        vb.setAlignment(Pos.CENTER);

        vb.setPadding(new Insets(40, 0, 0, 0));
        BorderPane layout = createBorderPane("Įveskite matricų A ir B duomenis į atitinkamus laukus", vb);
        layout.setAlignment(scrollPane, Pos.CENTER);

        btnSkaiciuoti.setOnAction(e -> {
            BigRational[][] matA = getElementsOfGridPane(tf1, Am, An);
            if(matA == null){
                new AlertBox("Neteisingai įvesti Matricos duomenys.");
            } else {
                BigRational rezultatas = super.determinant(matA);
                BigRational[][] result = new BigRational[1][1];
                result[0][0] = rezultatas;
                printResultToGridPane(result, tfRez);
            }
        });

        btnAtgal.setOnAction(e -> {
            loadScreen1();
        });

        Scene scene = new Scene(layout);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene determinantDimScene(){
        ComboBox<Integer> comboBoxMatAm = createComboBox();
        ComboBox<Integer> comboBoxMatAn = createComboBox();
        comboBoxMatAn.setDisable(true);

        comboBoxMatAm.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatAm.getValue());
        });
        comboBoxMatAn.setOnAction(e -> {
            comboBoxMatAm.setValue(comboBoxMatAn.getValue());
        });


        BorderPane border1 = createMatricosDimBorderPane("Matrica A", comboBoxMatAm, comboBoxMatAn, 5);

        Button btnAtgal = createButton("<Atgal");
        Button btnPirmyn = createButton("Pirmyn");

        btnAtgal.setOnAction(e -> loadScreen1());

        VBox v = new VBox();
        HBox centerHBox = new HBox();
        centerHBox.setSpacing(30);
        centerHBox.setAlignment(Pos.TOP_CENTER);
        centerHBox.getChildren().addAll(border1);

        VBox vBox = new VBox(btnAtgal);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(70,0,0,0));
        v.setSpacing(30);
        v.getChildren().addAll(centerHBox, btnPirmyn, vBox);
        v.setAlignment(Pos.TOP_CENTER);
        v.setPadding(new Insets(70, 0, 0, 0));

        btnPirmyn.setOnAction(e -> {
            int value = comboBoxMatAm.getValue();
            window.setScene(determinantRezScene(value, value));
        });


        HBox vb = new HBox(v);
        vb.setAlignment(Pos.CENTER);
        BorderPane layout = createBorderPane("Pasirinkite matricos A eilučių kiekį (m) bei stulpelių kiekį (n)", vb);
        Scene scene = new Scene(layout);

        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene x1DimsScene(){
        ComboBox<Integer> comboBoxMatAm = createComboBox();
        ComboBox<Integer> comboBoxMatAn = createComboBox();
        ComboBox<Integer> comboBoxMatBm = createComboBox();
        ComboBox<Integer> comboBoxMatBn = createComboBox();

        comboBoxMatAm.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatAm.getValue());
            comboBoxMatBm.setValue(comboBoxMatAm.getValue());
        });
        comboBoxMatAn.setOnAction(e -> {
            comboBoxMatAm.setValue(comboBoxMatAn.getValue());
            comboBoxMatBm.setValue(comboBoxMatAn.getValue());
        });
        comboBoxMatBm.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatBm.getValue());
            comboBoxMatAm.setValue(comboBoxMatBm.getValue());
        });
        comboBoxMatAn.setDisable(true);
        comboBoxMatBm.setDisable(true);

        BorderPane border1 = createMatricosDimBorderPane("Matrica A", comboBoxMatAm, comboBoxMatAn, 5);
        BorderPane border2 = createMatricosDimBorderPane("Matrica B", comboBoxMatBm, comboBoxMatBn, 1);

        Button btnAtgal = createButton("<Atgal");
        Button btnPirmyn = createButton("Pirmyn");

        btnAtgal.setOnAction(e -> loadScreen1());

        VBox v = new VBox();
        HBox centerHBox = new HBox();
        centerHBox.setSpacing(30);
        centerHBox.setAlignment(Pos.TOP_CENTER);
        Label labelDaugyba = new Label("x");
        labelDaugyba.setPadding(new Insets(30, 0, 0, 0));
        centerHBox.getChildren().addAll(border1, labelDaugyba, border2);

        VBox vBox = new VBox(btnAtgal);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(70,0,0,0));
        v.setSpacing(30);
        v.getChildren().addAll(centerHBox, btnPirmyn, vBox);
        v.setAlignment(Pos.TOP_CENTER);
        v.setPadding(new Insets(70, 0, 0, 0));

        btnPirmyn.setOnAction(e -> {
            int value = comboBoxMatAm.getValue();
            window.setScene(x1RezScene(value, value, value, comboBoxMatBn.getValue()));
        });

        HBox vb = new HBox(v);
        vb.setAlignment(Pos.CENTER);
        BorderPane layout = createBorderPane("Pasirinkite matricų eilučių kiekį (m) bei stulpelių kiekį (n)", vb);
        Scene scene = new Scene(layout);

        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene x1RezScene(int Am, int An, int Bm, int Bn){
        TextField[][] tf1 = new TextField[Am][An];
        TextField[][] tf2 = new TextField[Bm][Bn];
        TextField[][] tfRez = new TextField[Am][Bn];
        GridPane mat1 = createTable(Am, An, tf1);
        GridPane mat2 = createTable(Bm, Bn, tf2);
        GridPane rez = createTable(Am, Bn, true, tfRez);


        HBox hBox1 = createTable("Matrica A", Am, An, "x", mat1);
        HBox hBox2 = createTable("Matrica B", Bm, Bn, mat2);

        HBox finalHBox = new HBox();
        finalHBox.setSpacing(10);
        finalHBox.getChildren().addAll(hBox1, hBox2);
        finalHBox.setAlignment(Pos.CENTER);

        Button btnSkaiciuoti = createButton("Skaičiuoti");
        HBox bottom = createBottom(btnSkaiciuoti, rez);
        bottom.setAlignment(Pos.CENTER);
        VBox vBoxBottom = new VBox();
        vBoxBottom.setAlignment(Pos.CENTER);
        vBoxBottom.setSpacing(40);

        Button btnAtgal = createButton("<<Atgal");
        VBox v = new VBox(btnAtgal);
        vBoxBottom.getChildren().addAll(finalHBox, bottom, v);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBoxBottom);
        scrollPane.setStyle("-fx-background-color:transparent;");

        HBox vb = new HBox(scrollPane);
        vb.setAlignment(Pos.CENTER);

        vb.setPadding(new Insets(40, 0, 0, 0));
        BorderPane layout = createBorderPane("Įveskite matricų A ir B duomenis į atitinkamus laukus", vb);
        layout.setAlignment(scrollPane, Pos.CENTER);

        btnSkaiciuoti.setOnAction(e -> {
            BigRational[][] matA = getElementsOfGridPane(tf1, Am, An);
            BigRational[][] matB, result;
            if(matA == null){
                new AlertBox("Neteisingai įvesti duomenys Matricos A lauke.");
            } else {
                matB = getElementsOfGridPane(tf2, Bm, Bn);
                if(matB == null){
                    new AlertBox("Neteisingai įvesti duomenys Matricos B lauke.");
                } else {
                    BigRational[][] inversedA = super.inverse2(matA);
                    if(inversedA == null){
                        printResultToGridPane(null, tfRez);
                    } else {
                        result = super.sandauga(inversedA, matB);
                        printResultToGridPane(result, tfRez);
                    }
                }
            }
        });
        btnAtgal.setOnAction(e -> {
            loadScreen1();
        });

        Scene scene = new Scene(layout);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    private HBox createTable(String label, int m, int n, String centerSign, GridPane gridPane){
        VBox tblWithoutSign = new VBox();
        Label pavadinimas = new Label(label);
        Label MatLabelDims = new Label(m + "x" + n);
        MatLabelDims.setStyle("-fx-font-size: 10pt;");
        pavadinimas.setId("midText");

        tblWithoutSign.getChildren().addAll(pavadinimas, MatLabelDims, gridPane);
        tblWithoutSign.setAlignment(Pos.CENTER);

        VBox signVBox = new VBox();
        signVBox.getChildren().add(new Label(centerSign));
        signVBox.setAlignment(Pos.CENTER);

        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(tblWithoutSign, signVBox);

        return hBox;
    }

    private HBox createTable(String label, int m, int n, GridPane gridPane){
        VBox tblWithoutSign = new VBox();
        Label pavadinimas = new Label(label);
        Label MatLabelDims = new Label(m + "x" + n);
        MatLabelDims.setStyle("-fx-font-size: 10pt;");
        pavadinimas.setId("midText");

        tblWithoutSign.getChildren().addAll(pavadinimas, MatLabelDims, gridPane);
        tblWithoutSign.setAlignment(Pos.CENTER);


        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(tblWithoutSign);

        return hBox;
    }

    private HBox createTable(String label, int m, int n, String centerSign, GridPane gridPane, int kuriSpalva){
        VBox tblWithoutSign = new VBox();
        Label pavadinimas = new Label(label);
        Label MatLabelDims = new Label(m + "x" + n);
        MatLabelDims.setStyle("-fx-font-size: 10pt;");
        if(kuriSpalva == 1)
            pavadinimas.setId("color");
        else if(kuriSpalva == 2)
            pavadinimas.setId("color2");
        else
            pavadinimas.setId("midText");

        tblWithoutSign.getChildren().addAll(pavadinimas, MatLabelDims, gridPane);
        tblWithoutSign.setAlignment(Pos.CENTER);

        VBox signVBox = new VBox();
        signVBox.getChildren().add(new Label(centerSign));
        signVBox.setAlignment(Pos.CENTER);

        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(tblWithoutSign, signVBox);

        return hBox;
    }

    private HBox createTable(String label, int m, int n, GridPane gridPane, int kuriSpalva){
        VBox tblWithoutSign = new VBox();
        Label pavadinimas = new Label(label);
        Label MatLabelDims = new Label(m + "x" + n);
        MatLabelDims.setStyle("-fx-font-size: 10pt;");
        if(kuriSpalva == 1)
            pavadinimas.setId("color");
        else if(kuriSpalva == 2)
            pavadinimas.setId("color2");
        else
            pavadinimas.setId("midText");

        tblWithoutSign.getChildren().addAll(pavadinimas, MatLabelDims, gridPane);
        tblWithoutSign.setAlignment(Pos.CENTER);


        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(tblWithoutSign);

        return hBox;
    }

    private HBox createBottom(Button btnSkaiciuoti, GridPane gridPane){
        Label label = new Label("Rezultatas");
        label.setId("midText");
        VBox vbLabel = new VBox(label);
        vbLabel.setAlignment(Pos.CENTER);
        vbLabel.setPadding(new Insets(0, 0, 8, 0));

        VBox vb = new VBox();
        vb.getChildren().addAll(vbLabel, gridPane);

        VBox vBox = new VBox();
        vBox.getChildren().add(btnSkaiciuoti);
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.setPadding(new Insets(28, 0, 0, 0));

        HBox hBox = new HBox();
        hBox.setSpacing(20);
        hBox.getChildren().addAll(vBox, vb);

        return hBox;
    }

    public Scene x3RezScene(int Am, int An, int Bm, int Bn, int Cm, int Cn){
        TextField[][] tf1 = new TextField[Am][An];
        TextField[][] tf2 = new TextField[Bm][Bn];
        TextField[][] tf3 = new TextField[Cm][Cn];
        TextField[][] tfRez = new TextField[Am][Bn];
        GridPane mat1 = createTable(Am, An, tf1);
        GridPane mat2 = createTable(Bm, Bn, tf2);
        GridPane mat3 = createTable(Cm, Cn, tf3);
        GridPane rez = createTable(Am, Bn, true, tfRez);


        HBox hBox1 = createTable("Matrica A", Am, An, "x", mat1);
        HBox hBox2 = createTable("Matrica B", Bm, Bn, "x", mat2);
        HBox hBox3 = createTable("Matrica C", Cm, Cn, mat3);

        HBox finalHBox = new HBox();
        finalHBox.setSpacing(10);
        finalHBox.getChildren().addAll(hBox1, hBox2, hBox3);
        finalHBox.setAlignment(Pos.CENTER);

        Button btnSkaiciuoti = createButton("Skaičiuoti");
        HBox bottom = createBottom(btnSkaiciuoti, rez);
        bottom.setAlignment(Pos.CENTER);
        VBox vBoxBottom = new VBox();
        vBoxBottom.setAlignment(Pos.CENTER);
        vBoxBottom.setSpacing(40);

        Button btnAtgal = createButton("<<Atgal");
        VBox v = new VBox(btnAtgal);
        vBoxBottom.getChildren().addAll(finalHBox, bottom, v);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBoxBottom);
        scrollPane.setStyle("-fx-background-color:transparent;");

        HBox vb = new HBox(scrollPane);
        vb.setAlignment(Pos.CENTER);

        vb.setPadding(new Insets(40, 0, 0, 0));
        BorderPane layout = createBorderPane("Įveskite matricų A ir B duomenis į atitinkamus laukus", vb);
        layout.setAlignment(scrollPane, Pos.CENTER);

        btnSkaiciuoti.setOnAction(e -> {
            BigRational[][] matA = getElementsOfGridPane(tf1, Am, An);
            BigRational[][] matB, matC, result;
            BigRational[][] inversedA, inversedB;
            if(matA == null){
                new AlertBox("Neteisingai įvesti duomenys Matricos A lauke.");
            } else {
                matB = getElementsOfGridPane(tf2, Bm, Bn);
                if(matB == null){
                    new AlertBox("Neteisingai įvesti duomenys Matricos B lauke.");
                } else {
                    matC = getElementsOfGridPane(tf3, Cm, Cn);
                    if(matC == null) {
                        new AlertBox("Neteisingai įvesti duomenys Matricos C lauke.");
                    } else {
                        inversedA = super.inverse2(matA);
                        if (inversedA == null) {
                            printResultToGridPane(null, tfRez);
                        } else {
                            inversedB = super.inverse2(matB);
                            if(inversedB == null){
                                printResultToGridPane(null, tfRez);
                            } else {
                                result = super.sandauga(inversedA, matC);
                                result = super.sandauga(result, inversedB);
                                printResultToGridPane(result, tfRez);
                            }
                        }
                    }
                }
            }
        });

        btnAtgal.setOnAction(e -> {
            loadScreen1();
        });

        Scene scene = new Scene(layout);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene x2RezScene(int Am, int An, int Bm, int Bn){
        TextField[][] tf1 = new TextField[Am][An];
        TextField[][] tf2 = new TextField[Bm][Bn];
        TextField[][] tfRez = new TextField[Bm][An];
        GridPane mat1 = createTable(Am, An, tf1);
        GridPane mat2 = createTable(Bm, Bn, tf2);
        GridPane rez = createTable(Bm, An, true, tfRez);


        HBox hBox1 = createTable("Matrica A", Am, An, "x", mat1);
        HBox hBox2 = createTable("Matrica B", Bm, Bn, mat2);

        HBox finalHBox = new HBox();
        finalHBox.setSpacing(10);
        finalHBox.getChildren().addAll(hBox1, hBox2);
        finalHBox.setAlignment(Pos.CENTER);

        Button btnSkaiciuoti = createButton("Skaičiuoti");
        HBox bottom = createBottom(btnSkaiciuoti, rez);
        bottom.setAlignment(Pos.CENTER);
        VBox vBoxBottom = new VBox();
        vBoxBottom.setAlignment(Pos.CENTER);
        vBoxBottom.setSpacing(40);

        Button btnAtgal = createButton("<<Atgal");
        VBox v = new VBox(btnAtgal);
        vBoxBottom.getChildren().addAll(finalHBox, bottom, v);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBoxBottom);
        scrollPane.setStyle("-fx-background-color:transparent;");

        HBox vb = new HBox(scrollPane);
        vb.setAlignment(Pos.CENTER);

        vb.setPadding(new Insets(40, 0, 0, 0));
        BorderPane layout = createBorderPane("Įveskite matricų A ir B duomenis į atitinkamus laukus", vb);
        layout.setAlignment(scrollPane, Pos.CENTER);

        btnSkaiciuoti.setOnAction(e -> {
            BigRational[][] matA = getElementsOfGridPane(tf1, Am, An);
            BigRational[][] matB, result;
            if(matA == null){
                new AlertBox("Neteisingai įvesti duomenys Matricos A lauke.");
            } else {
                matB = getElementsOfGridPane(tf2, Bm, Bn);
                if(matB == null){
                    new AlertBox("Neteisingai įvesti duomenys Matricos B lauke.");
                } else {
                    BigRational[][] inversedA = super.inverse2(matA);
                    if(inversedA == null){
                        printResultToGridPane(null, tfRez);
                    } else {
                        result = super.sandauga(matB, inversedA);
                        printResultToGridPane(result, tfRez);
                    }
                }
            }
        });
        btnAtgal.setOnAction(e -> {
            loadScreen1();
        });

        Scene scene = new Scene(layout);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene x2DimsScene(){
        ComboBox<Integer> comboBoxMatAm = createComboBox();
        ComboBox<Integer> comboBoxMatAn = createComboBox();
        ComboBox<Integer> comboBoxMatBm = createComboBox();
        ComboBox<Integer> comboBoxMatBn = createComboBox();

        comboBoxMatAm.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatAm.getValue());
            comboBoxMatBn.setValue(comboBoxMatAm.getValue());
        });
        comboBoxMatAn.setOnAction(e -> {
            comboBoxMatAm.setValue(comboBoxMatAn.getValue());
            comboBoxMatBn.setValue(comboBoxMatAn.getValue());
        });
        comboBoxMatBn.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatBn.getValue());
            comboBoxMatAm.setValue(comboBoxMatBn.getValue());
        });

        comboBoxMatAn.setDisable(true);
        comboBoxMatBn.setDisable(true);

        BorderPane border1 = createMatricosDimBorderPane("Matrica A", comboBoxMatAm, comboBoxMatAn, 5);
        BorderPane border2 = createMatricosDimBorderPane("Matrica B", comboBoxMatBm, comboBoxMatBn, 2);

        Button btnAtgal = createButton("<Atgal");
        Button btnPirmyn = createButton("Pirmyn");

        VBox v = new VBox();
        HBox centerHBox = new HBox();
        centerHBox.setSpacing(30);
        centerHBox.setAlignment(Pos.TOP_CENTER);
        Label labelDaugyba = new Label("x");
        labelDaugyba.setPadding(new Insets(30, 0, 0, 0));
        centerHBox.getChildren().addAll(border1, labelDaugyba, border2);

        VBox vBox = new VBox(btnAtgal);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(70,0,0,0));
        v.setSpacing(30);
        v.getChildren().addAll(centerHBox, btnPirmyn, vBox);
        v.setAlignment(Pos.TOP_CENTER);
        v.setPadding(new Insets(70, 0, 0, 0));

        btnAtgal.setOnAction(e -> loadScreen1());
        btnPirmyn.setOnAction(e -> {
            int value = comboBoxMatAm.getValue();
            window.setScene(x2RezScene(value, value, comboBoxMatBm.getValue(), value));
        });

        HBox vb = new HBox(v);
        vb.setAlignment(Pos.CENTER);
        BorderPane layout = createBorderPane("Pasirinkite matricų eilučių kiekį (m) bei stulpelių kiekį (n)", vb);
        Scene scene = new Scene(layout);

        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene x3DimsScene(){
        ComboBox<Integer> comboBoxMatAm = createComboBox();
        ComboBox<Integer> comboBoxMatAn = createComboBox();
        ComboBox<Integer> comboBoxMatBm = createComboBox();
        ComboBox<Integer> comboBoxMatBn = createComboBox();
        ComboBox<Integer> comboBoxMatCm = createComboBox();
        ComboBox<Integer> comboBoxMatCn = createComboBox();

        comboBoxMatAm.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatAm.getValue());
            comboBoxMatCm.setValue(comboBoxMatAm.getValue());
        });
        comboBoxMatCn.setOnAction(e -> {
            comboBoxMatBm.setValue(comboBoxMatCn.getValue());
            comboBoxMatBn.setValue(comboBoxMatCn.getValue());
        });

        comboBoxMatAn.setDisable(true);
        comboBoxMatBm.setDisable(true);
        comboBoxMatBn.setDisable(true);
        comboBoxMatCm.setDisable(true);


        BorderPane border1 = createMatricosDimBorderPane("Matrica A", comboBoxMatAm, comboBoxMatAn, 5);
        BorderPane border2 = createMatricosDimBorderPane("Matrica B", comboBoxMatBm, comboBoxMatBn, 8);
        BorderPane border3 = createMatricosDimBorderPane("Matrica C", comboBoxMatCm, comboBoxMatCn, 6);

        Button btnAtgal = createButton("<Atgal");
        Button btnPirmyn = createButton("Pirmyn");


        //
        VBox v = new VBox();
        HBox centerHBox = new HBox();
        centerHBox.setSpacing(30);
        centerHBox.setAlignment(Pos.TOP_CENTER);
        Label labelDaugyba = new Label("x");
        labelDaugyba.setPadding(new Insets(30, 0, 0, 0));
        Label labelDaugyba2 = new Label("x");
        labelDaugyba2.setPadding(new Insets(30, 0, 0, 0));
        centerHBox.getChildren().addAll(border1, labelDaugyba, border2, labelDaugyba2, border3);

        VBox vBox = new VBox(btnAtgal);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(70,0,0,0));
        v.setSpacing(30);
        v.getChildren().addAll(centerHBox, btnPirmyn, vBox);
        v.setAlignment(Pos.TOP_CENTER);
        v.setPadding(new Insets(70, 0, 0, 0));

        btnAtgal.setOnAction(e -> loadScreen1());
        btnPirmyn.setOnAction(e -> {
            int value = comboBoxMatAm.getValue();
            int value2 = comboBoxMatCn.getValue();
            window.setScene(x3RezScene(value, value, value2, value2, value, value2));
        });

        HBox vb = new HBox(v);
        vb.setAlignment(Pos.CENTER);
        BorderPane layout = createBorderPane("Pasirinkite matricų eilučių kiekį (m) bei stulpelių kiekį (n)", vb);
        Scene scene = new Scene(layout);

        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    private ComboBox<Integer> createComboBox(){
        ComboBox<Integer> comboBox = new ComboBox<>();
        comboBox.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        comboBox.setValue(1);
        return comboBox;
    }

    private BorderPane createMatricosDimBorderPane(String matricosPav, ComboBox<Integer> m, ComboBox<Integer> n){
        BorderPane borderPane = new BorderPane();
        Label label = new Label(matricosPav);
        label.setId("midText");
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(label);

        borderPane.setTop(vBox);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        Label labelEil = new Label("Eilučių (m)");
        Label labelStulp = new Label("Stulpelių (n)");
        labelEil.setId("smallerText");
        labelStulp.setId("smallerText");

        gridPane.add(labelEil, 0, 0);
        gridPane.add(labelStulp, 1, 0);
        gridPane.add(m, 0, 1);
        gridPane.add(n, 1, 1);

        borderPane.setCenter(gridPane);

        return borderPane;
    }

    private BorderPane createMatricosDimBorderPane(String matricosPav, ComboBox<Integer> m, ComboBox<Integer> n, Label labelM, Label labelN){
        BorderPane borderPane = new BorderPane();
        Label label = new Label(matricosPav);
        label.setId("midText");

        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(label);

        borderPane.setTop(vBox);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);

        gridPane.add(labelM, 0, 0);
        gridPane.add(labelN, 1, 0);
        gridPane.add(m, 0, 1);
        gridPane.add(n, 1, 1);

        borderPane.setCenter(gridPane);

        return borderPane;
    }

    private BorderPane createMatricosDimBorderPane(String matricosPav, ComboBox<Integer> m, ComboBox<Integer> n, int kuriSpalvinam){
        BorderPane borderPane = new BorderPane();
        Label label = new Label(matricosPav);
        label.setId("midText");
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(label);

        borderPane.setTop(vBox);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        Label labelEil = new Label("Eilučių (m)");
        Label labelStulp = new Label("Stulpelių (n)");

        if(kuriSpalvinam == 0) {
            labelEil.setId("smallerText");
            labelStulp.setId("smallerText");
        } else if(kuriSpalvinam == 1){
            labelEil.setId("comboColor");
            labelStulp.setId("smallerText");
        } else if(kuriSpalvinam == 2){
            labelEil.setId("smallerText");
            labelStulp.setId("comboColor");
        } else if(kuriSpalvinam == 3){
            labelEil.setId("comboColor2");
            labelStulp.setId("smallerText");
        } else if(kuriSpalvinam == 4){
            labelEil.setId("smallerText");
            labelStulp.setId("comboColor2");
        } else if(kuriSpalvinam == 5){
            labelEil.setId("comboColor");
            labelStulp.setId("comboColor");
        } else if(kuriSpalvinam == 6){
            labelEil.setId("comboColor");
            labelStulp.setId("comboColor2");
        } else if(kuriSpalvinam == 7){
            labelEil.setId("comboColor2");
            labelStulp.setId("comboColor");
        } else {    //should be 8 here
            labelEil.setId("comboColor2");
            labelStulp.setId("comboColor2");
        }

        gridPane.add(labelEil, 0, 0);
        gridPane.add(labelStulp, 1, 0);
        gridPane.add(m, 0, 1);
        gridPane.add(n, 1, 1);

        borderPane.setCenter(gridPane);

        return borderPane;
    }

    private VBox createVBoxBottom(Button btnAtgal, Button btnPirmyn){
        VBox vBox = new VBox();

        VBox vBoxBtnPirmyn = new VBox();
        vBoxBtnPirmyn.getChildren().addAll(btnPirmyn);

        VBox vBoxBtnAtgal = new VBox();
        vBoxBtnAtgal.getChildren().addAll(btnAtgal);

        vBoxBtnAtgal.setPadding(new Insets(0, 0, 0, 70));
        vBoxBtnPirmyn.setAlignment(Pos.CENTER);
        vBoxBtnPirmyn.setPadding(new Insets(0, 0, 100, 0));

        vBox.getChildren().addAll(vBoxBtnPirmyn, vBoxBtnAtgal);
        vBox.setPadding(new Insets(0, 0, 30, 0));
        return vBox;
    }

    private BorderPane createBorderPane(String label, VBox center, VBox bottom){
        BorderPane layout = new BorderPane();
        layout.setPadding(new Insets(20, 0, 0, 0));
        VBox vBox = new VBox();
        Label text = new Label(label);
        vBox.getChildren().addAll(text);
        vBox.setAlignment(Pos.TOP_CENTER);
        layout.setTop(vBox);

        layout.setCenter(center);
        layout.setBottom(bottom);

        return layout;
    }

    private BorderPane createBorderPane(String label, HBox center){
        BorderPane layout = new BorderPane();
        layout.setPadding(new Insets(20, 0, 0, 0));
        VBox vBox = new VBox();
        Label text = new Label(label);
        vBox.getChildren().addAll(text);
        vBox.setAlignment(Pos.TOP_CENTER);
        layout.setTop(vBox);

        layout.setCenter(center);

        return layout;
    }

    private BorderPane createBorderPane(String label, ScrollPane center, VBox bottom){
        BorderPane layout = new BorderPane();
        layout.setPadding(new Insets(20, 0, 0, 0));
        VBox vBox = new VBox();
        Label text = new Label(label);
        vBox.getChildren().addAll(text);
        vBox.setAlignment(Pos.TOP_CENTER);
        layout.setTop(vBox);

        layout.setCenter(center);
        layout.setBottom(bottom);

        return layout;
    }

    private BorderPane createBorderPane(String label, HBox center, VBox bottom){
        BorderPane layout = new BorderPane();
        layout.setPadding(new Insets(20, 0, 0, 0));
        VBox vBox = new VBox();
        Label text = new Label(label);
        vBox.getChildren().addAll(text);
        vBox.setAlignment(Pos.TOP_CENTER);
        layout.setTop(vBox);

        layout.setCenter(center);
        layout.setBottom(bottom);

        return layout;
    }

    public Scene daugybosDimScene(){
        ComboBox<Integer> comboBoxMatAm = createComboBox();
        ComboBox<Integer> comboBoxMatAn = createComboBox();

        BorderPane border1 = createMatricosDimBorderPane("Matrica A", comboBoxMatAm, comboBoxMatAn, 2);

        ComboBox<Integer> comboBoxMatBm = createComboBox();
        ComboBox<Integer> comboBoxMatBn = createComboBox();
        comboBoxMatBm.setDisable(true);

        BorderPane border2 = createMatricosDimBorderPane("Matrica B", comboBoxMatBm, comboBoxMatBn, 1);

        comboBoxMatAn.setOnAction(e -> {
            comboBoxMatBm.setValue(comboBoxMatAn.getValue());
        });
        comboBoxMatBm.setOnAction(e -> {
            comboBoxMatAn.setValue(comboBoxMatBm.getValue());
        });

        Button btnAtgal = createButton("<Atgal");
        Button btnPirmyn = createButton("Pirmyn");
        //---------------------------
        VBox v = new VBox();
        HBox centerHBox = new HBox();
        centerHBox.setSpacing(30);
        centerHBox.setAlignment(Pos.TOP_CENTER);
        Label labelDaugyba = new Label("x");
        labelDaugyba.setPadding(new Insets(30, 0, 0, 0));
        centerHBox.getChildren().addAll(border1, labelDaugyba, border2);

        VBox vBox = new VBox(btnAtgal);
        vBox.setAlignment(Pos.TOP_LEFT);
        vBox.setPadding(new Insets(70,0,0,0));
        v.setSpacing(30);
        v.getChildren().addAll(centerHBox, btnPirmyn, vBox);
        v.setAlignment(Pos.TOP_CENTER);
        v.setPadding(new Insets(70, 0, 0, 0));

        btnAtgal.setOnAction(e -> loadScreen1());
        btnPirmyn.setOnAction(e -> window.setScene(daugybosRezScene(comboBoxMatAm.getValue(), comboBoxMatAn.getValue(), comboBoxMatBm.getValue(), comboBoxMatBn.getValue())));

        HBox vb = new HBox(v);
        vb.setAlignment(Pos.CENTER);
        BorderPane layout = createBorderPane("Pasirinkite matricų eilučių kiekį (m) bei stulpelių kiekį (n)", vb);
        Scene scene = new Scene(layout);

        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    public Scene daugybosRezScene(int Am, int An, int Bm, int Bn){
        TextField[][] tf1 = new TextField[Am][An];
        TextField[][] tf2 = new TextField[Bm][Bn];
        TextField[][] tfRez = new TextField[Am][Bn];
        GridPane mat1 = createTable(Am, An, tf1);
        GridPane mat2 = createTable(Bm, Bn, tf2);
        GridPane rez = createTable(Am, Bn, true, tfRez);


        HBox hBox1 = createTable("Matrica A", Am, An, "x", mat1);
        HBox hBox2 = createTable("Matrica B", Bm, Bn, mat2);

        HBox finalHBox = new HBox();
        finalHBox.setSpacing(10);
        finalHBox.getChildren().addAll(hBox1, hBox2);
        finalHBox.setAlignment(Pos.CENTER);

        Button btnSkaiciuoti = createButton("Skaičiuoti");
        HBox bottom = createBottom(btnSkaiciuoti, rez);
        bottom.setAlignment(Pos.CENTER);
        VBox vBoxBottom = new VBox();
        vBoxBottom.setAlignment(Pos.CENTER);
        vBoxBottom.setSpacing(40);

        Button btnAtgal = createButton("<<Atgal");
        VBox v = new VBox(btnAtgal);
        vBoxBottom.getChildren().addAll(finalHBox, bottom, v);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBoxBottom);
        scrollPane.setStyle("-fx-background-color:transparent;");

        HBox vb = new HBox(scrollPane);
        vb.setAlignment(Pos.CENTER);

        vb.setPadding(new Insets(40, 0, 0, 0));
        BorderPane layout = createBorderPane("Įveskite matricų A ir B duomenis į atitinkamus laukus", vb);
        layout.setAlignment(scrollPane, Pos.CENTER);

        btnSkaiciuoti.setOnAction(e -> {
            BigRational[][] matA = getElementsOfGridPane(tf1, Am, An);
            BigRational[][] matB, result;
            if(matA == null){
                new AlertBox("Neteisingai įvesti duomenys Matricos A lauke.");
            } else {
                matB = getElementsOfGridPane(tf2, Bm, Bn);
                if (matB == null) {
                    new AlertBox("Neteisingai įvesti duomenys Matricos B lauke.");
                } else {
                    result = super.sandauga(matA, matB);
                    printResultToGridPane(result, tfRez);
                }
            }
        });
        btnAtgal.setOnAction(e -> {
            loadScreen1();
        });

        Scene scene = new Scene(layout);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        return scene;
    }

    private Button createButton(String name){
        Button button = new Button(name);
        button.setId("alignCenter");
        button.setMinWidth(100);

        return button;
    }

    private TextArea setTextArea(int kelintas){
        TextArea inputField = new TextArea();
        inputField.setMinWidth(150);
        inputField.setMinHeight(150);
        inputField.setMaxHeight(150);
        inputField.setMaxWidth(150);

        if(kelintas == 1) {
            inputField.setPromptText("Pvz.,                                " + "2 -3.25 3/4                    " +
                    "-4.84 10/99 1/2         " +
                    "10/5 -9.4 7.3");
        } else if(kelintas == 2) {
            inputField.setPromptText("Matricos elementai atskiriami tarpo(space) arba 'enter' simboliais    " + "Pvz.,                                " + "2 -3.25 3/4 2/9 3/42       " +
                    "2 -3.25 3/4 2/9 3/42       " +
                    "10/5 -9.4 7.3 5 444");
        } else {
            inputField.setDisable(true);    //cia bus rez area, tad disablinta sukuriu.
        }

        return inputField;
    }

    private GridPane createTable(int m, int n, TextField[][] tf){
        GridPane root = new GridPane();
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                tf[i][j] = new TextField("") {
                    @Override public void replaceText(int start, int end, String text) {
                        // If the replaced text would end up being invalid, then simply
                        // ignore this call!
                        if (text.matches("[0-9./-]*")) {
                            super.replaceText(start, end, text);
                        }
                    }

                    @Override public void replaceSelection(String text) {
                        if (text.matches("[0-9./-]*")) {
                            super.replaceSelection(text);
                        }
                    }

                };
                tf[i][j].setPrefHeight(25);
                tf[i][j].setPrefWidth(50);
                tf[i][j].setMaxWidth(100);
                tf[i][j].setAlignment(Pos.CENTER);

                TextField tex = tf[i][j];   //jei buvo raudona ir buvo keiciamas skaicius, tai pasidaro nebe raudona.
                tex.setOnKeyTyped(e -> {
                    if(tex.getId() == "color"){
                        tex.setId("color2");
                    }
                });

                tex.textProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        double diff = tex.getWidth() - tex.getText().length() * 8.7;
                        if(diff < 0)
                            tex.setPrefWidth(tex.getText().length() * 8.7);
                    }
                });

                root.setRowIndex(tf[i][j], i);
                root.setColumnIndex(tf[i][j], j);
                root.getChildren().add(tf[i][j]);
            }
        }
        return root;
    }

    private GridPane createTable(int m, int n, boolean disableEditing, TextField[][] tf){
        GridPane root = new GridPane();
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                tf[i][j] = new TextField("");
                tf[i][j].setPrefHeight(25);
                tf[i][j].setPrefWidth(50);
                tf[i][j].setMaxWidth(150);
                tf[i][j].setAlignment(Pos.CENTER);
                tf[i][j].setEditable(false);

                TextField text = tf[i][j];

                text.textProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        double diff = text.getWidth() - text.getText().length() * 8.7;
                        if(diff < 0)
                            text.setPrefWidth(text.getText().length() * 8.7);
                    }
                });

                root.setRowIndex(tf[i][j], i);
                root.setColumnIndex(tf[i][j], j);
                root.getChildren().add(tf[i][j]);
            }
        }
        return root;
    }

    private VBox getAreaField(TextArea inputField, String matrica){
        Label pavadinimas = new Label(matrica);
        pavadinimas.setId("midText");

        VBox vBox = new VBox();
        vBox.setSpacing(8);
        vBox.getChildren().addAll(pavadinimas, inputField);
        vBox.setAlignment(Pos.BOTTOM_CENTER);
        return vBox;
    }

    private VBox getAreaField(TextArea inputField, String matrica, int matDimM, int matDimN){
        Label pavadinimas = new Label(matrica);
        Label MatLabelDims = new Label(Integer.toString(matDimM) + "x" + Integer.toString(matDimN));
        MatLabelDims.setStyle("-fx-font-size: 10pt;");
        pavadinimas.setId("midText");

        VBox vBox = new VBox();
        vBox.getChildren().addAll(pavadinimas, MatLabelDims, inputField);
        vBox.setAlignment(Pos.BOTTOM_CENTER);
        return vBox;
    }

    private BigRational[][] getElementsOfTextArea(TextArea textArea, int m, int n){
        String text = textArea.getText();
        String[] tokens = text.split("\\s+");
        return super.convertStrArrayToBigRational(tokens, m, n);
    }

    private BigRational[][] getElementsOfGridPane(TextField[][] tf, int m, int n){
        BigRational[][] bigRationals = new BigRational[m][n];
        boolean arBuvoRaudona = false;
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                String s = tf[i][j].getText();
                int tipas = super.checkIfNumber(s);
                if(tipas == -1){
                    tf[i][j].setId("color");
                    arBuvoRaudona = true;
                } else if(tipas == 1){
                    bigRationals[i][j] = new BigRational(BigInteger.valueOf(Integer.parseInt(s)), BigInteger.valueOf(1));
                } else if(tipas == 2){
                    bigRationals[i][j] = super.convertDoubleToBigRational(Double.parseDouble(s));
                } else {
                    bigRationals[i][j] = super.getBigRatFromFracString(s);
                }
            }
        }
        if(arBuvoRaudona)
            return null;

        return bigRationals;
    }

    private void printResultToGridPane(BigRational[][] matrica, TextField[][] tf){
        int m = tf.length;
        int n = tf[0].length;

        if(matrica == null){
            for(int i = 0; i < m; i++){
                for(int j = 0; j < n; j++){
                    tf[i][j].setText("nera spr");
                }
            }
        } else {
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    if (matrica[i][j].getDenominator() == BigInteger.valueOf(0)) {
                        tf[i][j].setText("0");
                    } else {
                        BigRational sk = BigRational.getInstance(matrica[i][j].getNumerator(), matrica[i][j].getDenominator());
                        if (sk.isInteger()) {
                            String s = String.valueOf(sk);
                            tf[i][j].setText(s);
                        } else {
                            String numerator = String.valueOf(sk.getNumerator());
                            String denominator = String.valueOf(sk.getDenominator());
                            tf[i][j].setText(numerator + "/" + denominator);
                        }
                    }
                }
            }
        }
    }

    private void printResultToTextArea(BigRational[][] matrica, TextArea textArea){
        if(matrica == null)
            return;

        String text = "";
        for(int i = 0; i < matrica.length; i++){
            for(int j = 0; j < matrica[0].length; j++){
                BigRational sk = BigRational.getInstance(matrica[i][j].getNumerator(), matrica[i][j].getDenominator());
                if(sk.isInteger()){
                    text += sk + " ";
                } else {
                    long numerator = sk.getNumerator().longValue();
                    long denominator = sk.getDenominator().longValue();
                    text += numerator + "/" + denominator + " ";
                }
            }
            text += "\n";
        }

        textArea.setDisable(false);
        textArea.setEditable(false);
        textArea.setText(text);
    }
}
