import javafx.scene.control.Alert;

public class AlertBox {
    //To-do: padaryti taip, jog issoktu ne monitoriaus viduryje, o to pagrindinio window viduryje.

    public AlertBox(String message){
        alert(message);
    }

    public void alert(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Neteisingi duomenys");
        alert.setHeaderText(message);
        alert.setContentText("Norint skaičiuoti, matrica privalo būti pilnai užpildyta.");

        alert.showAndWait();
    }
}
