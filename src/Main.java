import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {

        primaryStage.setWidth(1000);
        primaryStage.setHeight(750);
        Layouts layoutai = new Layouts(primaryStage);
        layoutai.loadScreen1();
    }
}
