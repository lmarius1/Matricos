import java.math.BigInteger;

/**
 * Created by Marius Lk on 2016-09-12.
 */
public class LinearProgramming {

    public BigRational[][] transpose(BigRational[][] mas){
        BigRational[][] newMas = new BigRational[mas.length][mas[0].length];

        for(int i = 0; i < mas.length; i++){
            for(int j = 0; j < mas[0].length; j++){
                newMas[j][i] = mas[i][j];
            }
        }

        return newMas;
    }

    public BigRational[][] sandauga(BigRational[][] masA, BigRational[][] masB) {
        // masA dauginamas is masB

        if (masA[0].length != masB.length) {
            System.out.println("Pirmo masyvo stulpeliu kiekis nesutampa su antro masyvo eiluciu kiekiu. Skaiciavimas nutraukiamas.");
            return null;
        }

        BigRational[][] rezultatas = new BigRational[masA.length][masB[0].length];  //sudaugintas masyvas bus lygus pirmo eiluciu ir antro stulpeliu kiekiui.

        for (int i = 0; i < masA.length; i++) {
            for (int j = 0; j < masB[0].length; j++) {
                rezultatas[i][j] = dauginkEiluteSuStulpeliu(masA, masB, i, j);
            }
        }

        //spausdink(rezultatas);

        return rezultatas;
    }

    private BigRational dauginkEiluteSuStulpeliu(BigRational[][] masA, BigRational[][] masB, int eilANr, int stulpBNr) {
        BigRational sum = new BigRational(BigInteger.valueOf(0), BigInteger.valueOf(0));
        for(int i = 0; i < masA[0].length; i++){
            sum = sum.add((masA[eilANr][i].multiply(masB[i][stulpBNr])));
        }

        return sum;
    }

    private void spausdink(BigRational[][] mas){
        System.out.println("------------------");
        System.out.println("Rezultatas:");
        for(int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[0].length; j++)
                System.out.print(BigRational.getInstance(mas[i][j].getNumerator(), mas[i][j].getDenominator()) + "  | ");
            System.out.println("");
        }
        System.out.println("");
    }

    public BigRational determinant(BigRational[][] matrix){ //method sig. takes a matrix (two dimensional array), returns determinant.
        BigRational sum = new BigRational(BigInteger.valueOf(0), BigInteger.valueOf(0));
        int s;
        if(matrix.length==1){  //bottom case of recursion. size 1 matrix determinant is itself.
            return matrix[0][0];
        }
        for(int i=0;i<matrix.length;i++){ //finds determinant using row-by-row expansion
            BigRational[][]smaller= new BigRational[matrix.length-1][matrix.length-1]; //creates smaller matrix- values not in same row, column
            for(int a=1;a<matrix.length;a++){
                for(int b = 0; b < i; b++){
                    smaller[a-1][b] = matrix[a][b];
                }
                for(int b = i+1; b < matrix.length; b++){
                    smaller[a-1][b-1] = matrix[a][b];
                }
            }
            BigRational newValue = matrix[0][i];
            newValue = newValue.multiply(determinant(smaller));
            if(i % 2 != 0)  //cia (-1)^(1+j). Tiktai kad atvirksciai tinka i%2, nes pradedam nuo 0.
                newValue = newValue.multiply(new BigRational(BigInteger.valueOf(-1), BigInteger.valueOf(1)));

            sum = sum.add(newValue);
        }
        return sum; //returns determinant value. once stack is finished, returns final determinant.
    }

    private BigRational[][] inverse(BigRational[][] mas){
        BigRational[][] naujasMas = new BigRational[mas.length][mas[0].length];
        BigRational[][] smaller = new BigRational[mas.length-1][mas.length-1];

        for(int i = 0; i < mas.length; i++){
            for(int j = 0; j < mas.length; j++){
                for(int a = 0; a < i; a++){
                    for(int b = 0; b < j; b++){
                        smaller[a][b] = mas[a][b];
                    }
                    for(int b = j+1; b < mas.length; b++){
                        smaller[a][b-1] = mas[a][b];
                    }
                }
                for(int a = i+1; a < mas.length; a++){
                    for(int b = 0; b < j; b++){
                        smaller[a-1][b] = mas[a][b];
                    }
                    for(int b = j+1; b < mas.length; b++){
                        smaller[a-1][b-1] = mas[a][b];
                    }
                }

                naujasMas[i][j] = determinant(smaller);

                if((i+j)%2 != 0)            //cia del to (-1)^(i+j). Kai kur reikia padauginti is -1, kadangi kiti i ir j...
                    naujasMas[i][j] = naujasMas[i][j].negate();
                //System.out.print(naujasMas[i][j] + " ");
            }
            //System.out.println("");
        }

        return naujasMas;
    }

    public BigRational[][] inverse2(BigRational[][] mas){
        if(mas.length == 1 && mas[0].length == 1){  //jei dydis tik vienas, tuomet 'inverse' metodas netinka, nes ten nesumazins masyvo ir returnins 0. Jei kartais paaisketu, kad klaidingi atsakymai, tai visu pirma ten ieskoti buggo.
            if(mas[0][0].getDenominator() == BigInteger.valueOf(0) || mas[0][0].getNumerator() == BigInteger.valueOf(0)){     //dalyba is nulio negalima, tad tokiu atveju returninu null
                return null;
            }
            mas[0][0] = mas[0][0].invert();
            return mas;
        }
        BigRational[][] determinantai = inverse(mas);
        determinantai = transpose(determinantai);
        BigRational detA = determinant(mas);
        if(detA.getDenominator() == BigInteger.valueOf(0) || detA.getNumerator() == BigInteger.valueOf(0)){     //dalyba is nulio negalima, tad tokiu atveju returninu null
            return null;
        }
        BigRational fraction = new BigRational(BigInteger.valueOf(1), BigInteger.valueOf(1));
        fraction = fraction.divide(detA);
        BigRational[][] doubleDeterminantai = new BigRational[determinantai.length][determinantai.length];

        for(int i = 0; i < mas.length; i++){
            for(int j = 0; j < mas.length; j++){
                doubleDeterminantai[i][j] = determinantai[i][j].multiply(fraction);
                //System.out.print(doubleDeterminantai[i][j] + " ");
            }
            //System.out.println("");
        }

        return doubleDeterminantai;
    }

    protected BigRational convertDoubleToBigRational(double number){
        String s = String.valueOf(number);
        int digitsDec = s.length() - 1 - s.indexOf('.');
        int denom = 1;
        for (int i = 0; i < digitsDec; i++) {
            number *= 10;
            denom *= 10;
        }

        int num = (int) number;
        BigRational bigRational = new BigRational(BigInteger.valueOf(num), BigInteger.valueOf(denom));

        return bigRational;
    }

    protected int checkIfNumber(String num){  //patikrinama ar given string galima konvertuoti i skaiciu. Jei taip, koki (integer, double, fractional)
        if(num.length() == 0)
            return -1;
        if(num.charAt(0) != 45 && (num.charAt(0) < 48 || num.charAt(0) > 57))   //jei pirmas skaicius nera nei '-', nei skaicius, tuomet tai nera skaicius.
            return -1;

        boolean arJauSeperated = false; //reikalingas tam, kad patikrintu '.' ar '/' simbolius. Kadangi sk gali buti 1.22 arba 1/2 pvz.
        int kurisAtskirtas = 0; //pasakys ar buvo pagautas '.', ar '/' simbolis.

        for(int i = 1; i < num.length() -1; i++){
            char tmp = num.charAt(i);
            if(tmp < 48 || tmp > 57){
                if(arJauSeperated)
                    return -1;
                if(tmp == 46) {     //pasizymiu, kad atskirtas '.' simbolis.
                    arJauSeperated = true;
                    kurisAtskirtas = 1;
                } else if(tmp == 47) {
                    arJauSeperated = true;
                    kurisAtskirtas = 2;
                } else
                    return -1;      //cia reiskia, jog
            }
        }
        char lastChar = num.charAt(num.length()-1);
        if(lastChar < 48 || lastChar > 57)
            return -1;

        if(arJauSeperated == false) //returning integer
            return 1;

        if(kurisAtskirtas == 1)     //returning double
            return 2;

        for(int i = 1; i < num.length()-1; i++){    //dalyba is 0 negalima
            if(num.charAt(i) == 47){
                if(num.charAt(i+1) == '0'){
                    return -1;
                }
                return 3;
            }
        }

        return 3;               //returning fraction
    }

    public BigRational[][] convertStrArrayToBigRational(String[] stringArray, int m, int n){
        if(stringArray.length != m*n){
            //System.out.println("Per mazai arba per daug elementu");
            return null;
        }
        BigRational[][] bigRationals = new BigRational[m][n];
        int i = 0, j =0;

        for(String s : stringArray){
            int tipas = checkIfNumber(s);
            if(tipas == -1){
                //System.out.println("Neteisingas elemento tipas");
                return null;
            }
            if(tipas == 1)
                bigRationals[i][j] = new BigRational(BigInteger.valueOf(Integer.parseInt(s)), BigInteger.valueOf(1));
            else if(tipas == 2)
                bigRationals[i][j] = convertDoubleToBigRational(Double.parseDouble(s));
            else
                bigRationals[i][j] = getBigRatFromFracString(s);

            j++;
            if(j == n){
                j = 0;
                i++;
            }
        }
        return bigRationals;
    }

    //siaip cia visuomet turetu splitintas but ilgio 2, kadangi yra paleidziamas tik pracheckintas su 'check if number' funkcija, kuri returnina 3 (kas reiskia exactly 1 '/').
    protected BigRational getBigRatFromFracString(String frac){
        String[] tokens = frac.split("/");
        if(tokens.length == 2){
            BigRational bigRational = new BigRational(BigInteger.valueOf(Integer.parseInt(tokens[0])), BigInteger.valueOf(Integer.parseInt(tokens[1])));
            return  bigRational;
        }
        return null;
    }
}